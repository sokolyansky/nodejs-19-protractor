'use strict';

global.chai = require('chai');
const promised = require('chai-as-promised');
chai.use(promised);

global.expect = global.chai.expect;
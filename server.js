'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
let db; // сохраняю переменную базы данных в глобальной области для доступа из функций модуля
const port = process.env.PORT || 3000;


// раздача статики через express
app.use(express.static(__dirname + '/app'));


app.listen(port, function () {
  console.log('Start HTTP server on %d port', port);
});
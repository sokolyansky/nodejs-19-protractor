'use strict';

angular
  .module('myApp')
  .controller('MyAccountCtrl', function (MyAccountStore) {
    const vm = this;

    vm.myInfo = MyAccountStore.getData();

    vm.addToMyAccount = function (info) {
      MyAccountStore.addData(info);
    }
  });

describe.skip('AngularJS home page', () => {

  it('should have a default title', () => {
    browser.get('http://angularjs.org');
    const title = browser.getTitle(); // возвращает Promise поэтому нужно использовать eventually из библиотеки chai-as-promised

    expect(title).to.eventually.equal('AngularJS — Superheroic JavaScript MVW Framework');
  });

  it('my project page', () => {
    browser.get('http://localhost:3000');
    const title = browser.getTitle();

    expect(title).to.eventually.equal('My AngularJS App');
  })
});

# test client by Protractor
Demo files for Netology Node, Angular, MongoDB Course. Lesson 19
Based on lesson 14

1. npm install
2. node server (launch express server which distribute statics. Do it in particular bash)
3. npm run web-driver (launch Selenium server. Do it in particular bash)
4. npm test  (start protractor test. Do it in particular bash too)

In total, the process needs 3 opened bashes.
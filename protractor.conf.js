exports.config = {
  framework: 'mocha',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['test/*.js'],
  capabilities: {
    browserName: 'chrome'
  },
  mochaOpts: {
    reporter: 'spec',
    timeout: 10000,
    slow: 10000,
    require: './test/common.js'
  }
};

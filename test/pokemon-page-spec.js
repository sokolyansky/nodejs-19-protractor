describe('Pokemon Application', () => {
  const pokemonUrl = 'http://localhost:3000';

  var hasClass = function (element, cls) {
    return element.getAttribute('class').then(function (classes) {
      return classes.split(' ').indexOf(cls) !== -1;
    });
  };

  it('should have a highlight of a List tab', () => {
    browser.get(pokemonUrl);

    const tabs = element.all(by.repeater('button in $ctrl.buttons'));
    const tab = tabs.get(0);
    tab.click();

    expect(hasClass(tab, 'btn-primary')).to.eventually.equal(true);
  });

  describe('test form /myaccount', () => {

    beforeEach(() => {
      browser.get('http://localhost:3000/#!/myaccount');
    });

    it('should open it', () => {
      const formName = element(by.tagName('form')).getAttribute('name');
      expect(formName).to.eventually.equal('account');
    });

    it('test required fields', () => {
      const name = element(by.model('vm.myInfo.name'));
      const email = element(by.model('vm.myInfo.email'));
      const save = element(by.tagName('button'));

      name.sendKeys('Dmitry');
      email.sendKeys('sokol@gmail.com');
      save.click();

      expect(name.getAttribute('value')).to.eventually.equal('Dmitry');
      expect(email.getAttribute('value')).to.eventually.equal('sokol@gmail.com');
    });
  });

  describe('test /list', () => {

    beforeEach(() => {
      browser.get('http://localhost:3000/#!/list');
    });

    it('add pokemon into the cart', () => {

      const shoppingCart = element(by.tagName('shopping-cart-component'));
      const add = element(by.tagName('button'));

      add.click();

      expect(shoppingCart.element(by.css('.list-group-item')).isPresent()).to.eventually.equal(true);
    });

    it('test pokemon list', () => {

      const pokemons = element.all(by.repeater('singlePokemon in vm.pokemons')).count();

      expect(pokemons).to.eventually.equal(835);
    });

  });

});
